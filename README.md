# PKGBUILD files #

A collection of my PKGBUILD files for Arch User Repository.

## Contents ##

  * understand-bin

  Understand™ Static Code Analysis Tool is an IDE built from the ground up to
  help you fully comprehend your source code. Analyze it, measure it, visualize
  it, maintain it - Understand it.

  * bleah-python2-git

  A BLE scanner for "smart" devices hacking based on the bluepy library, dead
  easy to use because retarded devices should be dead easy to hack.
